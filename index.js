console.info("Hello");

//[Section] Mutator
let fruits = ["Apple", "Orange", "Kiwi", "Dragon fruit"];
console.log(fruits);
    
    // push()
    // -Adds an element in the end of an array and returns the array's length
    //Syntax:
    //   arrayName.push(elementsToBeAdded);
    console.log("Current Array Fruits[]:")
    console.info(fruits);

    let fruitsLength = fruits.push('Mango');
    console.log('Mutated array from push method:')
    console.info(fruits);

    // adding multiple elements to an array.
    fruits.length = fruits.push("Avocado", "Guava");
    console.log("Mutated array after push method:");
    console.log(fruits);
    console.log(fruitsLength);

    // pop()
        // removes the last element in an array
        //  Syntax:
        //     arrayName.pop();
        console.log("Current Array Fruits[]:");
        console.log(fruits);
        let removedFruit = fruits.pop();
        console.log("Mutated after POP method:");
        console.log(fruits);
        console.log(removedFruit);

        //  -Adds one or more elements at the begining of an syntax
    //      Syntax:
    //          arrayName.unshift('elementA);
    //          arrayName.unshift('elementA','elementB');
    
    // unshift()
    console.log("Current Array fruits[]:");
    console.log(fruits);

    fruitsLength= fruits.unshift("Lime", 'Banana');
    console.log("Mutated Array After the unshift() method:")
    console.log(fruits);
    console.log(fruitsLength);

    // shift()
    //  -removes an element at the beginning of an array and also it returns the removed element.
    //  -Syntax:
    //      arrayName.shift()

    console.log("Current Array fruits[]:");
    console.log(fruits);

    removedFruit =  fruits.shift();
    console.log("Mutated Array After the shift() method:")
    console.log(fruits);
    console.log(removedFruit);

    // splice()
    //  simultaneously removes elements from a specified index number and adds elements.
    //      Syntax:
    //          arrayName.splice(startingIndex, deletecount, elementsToBedded);
                // splice return removed
    console.log("Current Array fruits[]:");
    console.log(fruits);

    fruits.splice(1,1, 'Lime');
    console.log("Mutated Array After the spliced() method:")
    console.log(fruits);

    fruits.splice(1,0, "Cherry");
    console.log(fruits);

    console.log("Current Array fruits[]:");
    console.log(fruits);

    fruits.splice(3,0,"Durian","Santol");
    console.log("Mutated Array After the spliced(3,0,'Durian','Santol') method:")
    console.log(fruits);

    // sort()
        // rearranges the array elements in alphanumerical order
        // syntax:
        // arrayName.sort();

    console.log("Current Array fruits[]:");
    console.log(fruits);
    console.log(fruits.sort());
    console.log("Mutated Array After the sort() method:")
    console.log(fruits);


    // Important Note
    // reverser()
    //  -reversed the order of array elements

    // arrayName.reverse();
    console.log("Current Array fruits[]:");
    console.log(fruits);
    console.log(fruits.reverse());
    console.log("Mutated Array After the reverse() method:")
    console.log(fruits);

    // [Section] Non-Mutator Methods
    // 
    let countries = ['USA', 'PH','SG','TH', 'PH', 'FR', 'DE'];

        // indexof()
        // it returns the index number of the first matching elements found in an array
        // if no match was found, the result will be -1
        // the search process will be dont from first element proceeding to the last element.
        //  Sntax:
        //       arrayName.indexOf(searchValue);
        //      arrayName.indexof(searchvalue, startingIndex);

        console.log(countries.indexOf('PH'));
        console.log(countries.indexOf('BR'));
        
        console.log(countries.indexOf('PH',2));

        // lastIndexOf()
        // returns the index number of the last matching element found in an array
        // the search process will be done from last element proceeding to the first element.
            // arrayName.lastIndexOf(searchValue);
            // arrayname.lastIndexOf(searchValue,startingIndex);
        console.log(countries.lastIndexOf('PH'));

        // slice()
        //  portion/slices from and array and returns a new array
        //  Syntax:
            // arrayName.slice(startingIndex);
            // arrayName.slice(startingIndex, endingIndex);

        let slicedArrayA=countries.slice(2);
        console.log(slicedArrayA);
        console.log(countries);

        // Slicing off elements
        let slicedArrayB=countries.slice(1,5);
        console.log(slicedArrayB)

        // slicing off elements starting from the last element of an array

        let slicedArrayC= countries.slice(-3);
        console.log(slicedArrayC);

        // toString()
        // return an array as String seperated by commas
        //      syntax:
        //      arraynme
        let stringedArray = countries.toString();
        console.log(stringedArray);

        // concat()
            // combines arrays and returns the combined result
            //  Syntax:
            //    ArrayA.concat(arrayB);
            //    arrayA.Concat(elementsA);

            let taskArrayA = ['drink Html', 'eat JavaScript'];
            let taskArrayB = ['inhale CSS', 'breathe SASS'];
            let taskArrayC = ['get git', 'be Node'];

            let tasks=taskArrayA.concat(taskArrayB);
            console.log('result from concat method:')
            console.log(tasks);

            console.log("Result from concat() method:");
            let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
            console.log(allTasks);
            // combine array with elements
            let combinedTasks = taskArrayA.concat('smell express', 'throw react');
            console.log(combinedTasks);